<?php
/**
 * Created by PhpStorm.
 * Date: 27/03/2019
 * Time: 19:34
 */

namespace App\Security;


use App\Entity\Projet;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProjetVoter extends Voter
{

    const VIEW= 'view';
    const EDIT= 'edit';

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        /** @var Projet $projet */
        $projet = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($projet, $user);
            case self::EDIT:
                return $this->canEdit($projet, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }
    private function canView(Projet $projet, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($projet, $user)) {
            return true;
        }

        // the Post object could have, for example, a method isPrivate()
        // that checks a boolean $private property
        return !$projet->isPrivate();
    }

    private function canEdit(Projet $projet, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $projet->getChef();
    }
}