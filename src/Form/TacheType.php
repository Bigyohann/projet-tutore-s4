<?php

namespace App\Form;

use App\Entity\Tache;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TacheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $projet = $options['projet'];
        $builder
            ->add('name')
            ->add('description')
            ->add('assignedto', EntityType::class, array(
                    'query_builder' => function (UserRepository $userRepository) use ($projet) {
                        return $userRepository->createQueryBuilder('ur')
                            ->where(":projet MEMBER OF ur.memberof")
                            ->setParameter('projet', $projet);
                    },
                    'class' => User::class
                )
            )
            ->add('datefin', DateType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tache::class,
            'projet' => null
        ]);
    }
}
