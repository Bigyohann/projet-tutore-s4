<?php

namespace App\Form;

use App\Entity\Tableau;
use App\Entity\Tache;
use App\Repository\TableauRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoveTacheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $projet = $options['projet'];
        $builder
            ->add('tableau', EntityType::class, array(
                    'query_builder' => function (TableauRepository $tableauRepository) use ($projet) {
                        return $tableauRepository->createQueryBuilder('tr')
                            ->where('tr.projet = :projet')
                            ->setParameter('projet', $projet);
                    },
                    'class'=> Tableau::class
                )
            );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tache::class,
            'projet' => null
        ]);
    }
}
