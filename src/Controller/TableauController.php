<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Entity\Tableau;
use App\Entity\Tache;
use App\Form\TableauType;
use App\Form\TacheType;
use App\Repository\TableauRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tableau")
 */
class TableauController extends AbstractController
{
    /**
     * @Route("/", name="tableau_index", methods={"GET"})
     */
    public function index(TableauRepository $tableauRepository): Response
    {
        return $this->render('tableau/index.html.twig', [
            'tableaus' => $tableauRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tableau_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tableau = new Tableau();
        $form = $this->createForm(TableauType::class, $tableau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($tableau->getTaches() as $taches) {
                $taches->setTableau($tableau);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tableau);
            $entityManager->flush();

            return $this->redirectToRoute('projet_show', array(
                'id' => $tableau->getProjet()->getId(),
            ));
        }

        return $this->render('tableau/new.html.twig', [
            'tableau' => $tableau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tableau_show", methods={"GET"})
     */
    public function show(Tableau $tableau): Response
    {
        return $this->render('tableau/show.html.twig', [
            'tableau' => $tableau,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tableau_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tableau $tableau): Response
    {
        $form = $this->createForm(TableauType::class, $tableau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($tableau->getTaches() as $taches) {
                $taches->setTableau($tableau);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_show', array(
                'id' => $tableau->getProjet()->getId(),
            ));
        }

        return $this->render('tableau/edit.html.twig', [
            'tableau' => $tableau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tableau_delete", methods={"DELETE"})
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request, Tableau $tableau): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tableau->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tableau);
            $entityManager->flush();
        }

        return $this->redirectToRoute('projet_show', array(
            'id' => $tableau->getProjet()->getId(),
        ));
    }

    /**
     * @Route("/{id}/{tableau}/nouvelletache", name="tache_new", methods={"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function tacheAdd(Request $request, Tableau $tableau, Projet $projet)
    {
        $tache = new Tache();
        $form = $this->createForm(TacheType::class, $tache, array(
            'projet' => $projet
        ));
        $form->handleRequest($request);

        dump($form);
        if ($form->isSubmitted() && $form->isValid()) {

            $tache->setTableau($tableau);

            $tache->setDatedebut(new DateTime('now'));

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($tache);
            $entityManager->flush();

            return $this->redirectToRoute('projet_show', array(
                'id' => $tableau->getProjet()->getId(),
            ));
        }

        return $this->render('taches/add.html.twig', [
            'tache' => $tache,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{tableau}/{tache}", name="tache_edit", methods={"POST", "GET"})
     * @param Request $request
     * @param Tableau $tableau
     * @param Tache $tache
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function tacheEdit(Request $request, Tableau $tableau, Tache $tache, Projet $projet)
    {
        $form = $this->createForm(TacheType::class, $tache, array(
            'projet' => $projet
        ));
        $form->handleRequest($request);

        dump($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();


            return $this->redirectToRoute('projet_show', array(
                'id' => $tableau->getProjet()->getId(),
            ));
        }

        return $this->render('taches/edit.html.twig', [
            'tache' => $tache,
            'form' => $form->createView(),
        ]);
    }
}
