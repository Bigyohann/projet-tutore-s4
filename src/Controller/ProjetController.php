<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Entity\Tableau;
use App\Form\ProjetType;
use App\Repository\ProjetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/projet")
 */
class ProjetController extends AbstractController
{
    /**
     * @Route("/", name="projet_index", methods={"GET"})
     */
    public function index(ProjetRepository $projetRepository): Response
    {
        return $this->render('projet/index.html.twig', [
            'projets' => $projetRepository->findAll(),
        ]);
    }

    /**
     * Undocumented function
     * 
     * @Route("/index")
     *
     * @return void
     */
    public function indexlol(ProjetRepository $projetRepository){
        return render('projet/index.html.twig', array(
            'projets' => $projetRepository->findAll()
        ));
    }

    /**
     * @Route("/new", name="projet_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $projet = new Projet();
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $colonnetodo = new Tableau();
            $colonnetodo->setProjet($projet);
            $colonnetodo->setName("Todo");

            $colonnedoing = new Tableau();
            $colonnedoing->setProjet($projet);
            $colonnedoing->setName("Doing");

            $colonnedone = new Tableau();
            $colonnedone->setProjet($projet);
            $colonnedone->setName("Done");

            $projet->addTableaux($colonnetodo);
            $projet->addTableaux($colonnedoing);
            $projet->addTableaux($colonnedone);

            $projet->setChef($this->getUser());
            $projet->addMembre($this->getUser());
            dump([$form, $projet]);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($projet);
            $entityManager->flush();

            return $this->redirectToRoute('projet_index');
        }

        return $this->render('projet/new.html.twig', [
            'projet' => $projet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="projet_show", methods={"GET"})
     */
    public function show(Projet $projet): Response
    {
        return $this->render('projet/show.html.twig', [
            'projet' => $projet,
        ]);
    }


    /**
     * @Route("/{id}/edit", name="projet_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Projet $projet): Response
    {
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $projet->addMembre($this->getUser());
            dump([$form, $projet]);
            $this->getDoctrine()->getManager()->flush();


            return $this->redirectToRoute('projet_index', [
                'id' => $projet->getId(),
            ]);
        }

        return $this->render('projet/edit.html.twig', [
            'projet' => $projet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="projet_delete", methods={"DELETE"})
     * @param Request $request
     * @param Projet $projet
     * @return Response
     */
    public function delete(Request $request, Projet $projet): Response
    {
        if ($this->isCsrfTokenValid('delete' . $projet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($projet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('projet_index');
    }

}
