<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

class Site extends AbstractController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="Accueil")
     */
    public function getAccueil(){

        dump($this->getUser());

        return $this->render('site/accueil.html.twig',array(
            'accueil' => true,
        ));
    }
}