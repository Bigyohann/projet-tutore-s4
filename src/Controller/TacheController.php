<?php

namespace App\Controller;



use App\Entity\Projet;
use App\Entity\Tache;
use App\Form\MoveTacheType;
use App\Repository\TacheRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class TacheController
 * @package App\Controller
 * @Route("/tache")
 */
class TacheController extends AbstractController
{

    /**
     * @param TacheRepository $tacheRepository
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexTaches(TacheRepository $tacheRepository){

        $user= $this->getUser();
        dump($user);

        return $this->render('taches/index.html.twig',array(
            'taches' => $tacheRepository->findBy(array(
                'assignedto' => $this->getUser()
            ))
        ));
    }

    /**
     * @Route("/{projet}/{tache}/move", name="tache_move", methods={"GET","POST"})
     * @param Request $request
     * @param Projet $projet
     * @param Tache $tache
     * @return Response
     */
    public function move(Request $request, Projet $projet, Tache $tache): Response
    {
        $form = $this->createForm(MoveTacheType::class, $tache, array(
            'projet' => $projet
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_show', array(
                'id' => $projet->getId(),
            ));
        }

        return $this->render('taches/move.html.twig', [
            'tache' => $tache,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tache_delete", methods={"DELETE"})
     * @param Request $request
     * @param Tache $tache
     * @return Response
     */
    public function delete(Request $request, Tache $tache): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tache->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tache);
            $entityManager->flush();
        }

        return $this->redirectToRoute('projet_show', array(
            'id' => $tache->getTableau()->getProjet()->getId(),
        ));
    }


}